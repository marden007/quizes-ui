var myApp = angular.module('myApp', []);
myApp.controller('TestCtrl', [
  '$scope',
  '$http',
  '$timeout',
  function ($scope, $http, $timeout) {

    var searchQueries = location.search.replace('?', '').split('&').reduce(function(full, item) {
      var kv = item.split('=');

      full[kv[0]] = kv[1];

      return full;
    }, {});

    $scope.quizParams = {
      userEmail: searchQueries.email || '@gmail.ru'
    };

    $scope.questions = [
      {
        question_id: 0,
        text: "Объявлена функция. Чем является F.prototype?",
        type: 2,
        code: 'function F() { }',
        answers: [
          {text: "Обычным объектом", isSelected: false, isCorrect:true},
          {text: "Равен null", isSelected: false},
          {text: "Функцией", isSelected: false},
          {text: "Равен undefined", isSelected: false}]
      },
      {
        question_id: 1,
        text: "Чему равно arr.length?",
        type: 2,
        code: 'function MyArray() { }\n'
              +'MyArray.prototype = [];\n\n'
              +'var arr = new MyArray();\n'
              +'arr.push(1, 2, 3);\n'
              +'alert(arr.length);',
        answers: [
          {text: "0", isSelected: false},
          {text: "undefined", isSelected: false},
          {text: "3", isSelected: false, isCorrect:true},
          {text: "В этом коде допущена ошибка", isSelected: false}]
      },
      {
        question_id: 2,
        text: "Что вернет следующий код?",
        type: 2,
        code: '~~(-5.5)',
        answers: [
          {text: "5", isSelected: false},
          {text: "-6", isSelected: false},
          {text: "-5", isSelected: false, isCorrect:true},
          {text: "Error", isSelected: false}]
      },
      {
        question_id: 3,
        text: "Что вернет следующий код?",
        type: 2,
        code: '(function f(f) {\n'
                +'return typeof f();\n'
                +'}(function () {\n'
                +'return 1;\n'
                +'}));',
        answers: [
          {text: "number", isSelected: false, isCorrect: true},
          {text: "undefined", isSelected: false},
          {text: "function", isSelected: false},
          {text: "Error", isSelected: false}]
      },
      {
        question_id: 4,
        text: "В каком порядке выведутся значения?",
        type: 2,
        code: 'var x = 3;\n'
        +'var foo = {\n'
        +'  x: 2,\n'
        +'  baz: {\n'
        +'    x: 1,\n'
        +'    bar: function() {\n'
        +'      return this.x;\n'
        +'    }\n'
        +'  }\n'
        +'}\n'
        +'var go = foo.baz.bar;\n'
        +'alert(go());\n'
        +'alert(foo.baz.bar());\n',
        answers: [
          {text: "1, 2", isSelected: false},
          {text: "1, 3", isSelected: false},
          {text: "2, 1", isSelected: false},
          {text: "3, 1", isSelected: false, isCorrect: true},
          {text: "3, 2", isSelected: false}]
      },
      {
        question_id: 5,
        text: "Верно ли, что null == undefined?",
        type: 2,
        answers: [
          {text: "Да", isSelected: false, isCorrect: true},
          {text: "Нет", isSelected: false}]
      },
      {
        question_id: 6,
        text: "Через что передаются данные из родительского компонента в дочерний?",
        type: 2,
        answers: [
          {text: "state", isSelected: false},
          {text: "props", isSelected: false, isCorrect: true}]
      },
      {
        question_id: 7,
        text: "Как установить props по умолчанию у компонента?",
        type: 2,
        answers: [
          {text: "getDefaultProps", isSelected: false, isCorrect: true},
          {text: "getPropsValue", isSelected: false},
          {text: "getInitialState", isSelected: false}]
      },
      {
        question_id: 8,
        text: "Какой из методов React-компонента отвечает за то, должен ли компонент обновиться?",
        type: 2,
        answers: [
          {text: "componentWillUpdate()", isSelected: false},
          {text: "shouldComponentUpdate()", isSelected: false, isCorrect: true},
          {text: "componentWillMount()", isSelected: false},
          {text: "shouldComponentMount()", isSelected: false}]
      },
      {
        question_id: 9,
        text: "В каком пункте перечислены категории жизненного цикла компонента?",
        type: 2,
        answers: [
          {text: "Mounting, Unmounting", isSelected: false},
          {text: "Mounting, Updating", isSelected: false},
          {text: "Mounting, Updating, Unmounting", isSelected: false, isCorrect: true}]
      },
      {
        question_id: 10,
        text: "Какой из перечисленных примеров представляет собой валидный action редакса?",
        type: 2,
        answers: [
          {text: "{"
                 +"   action:'LOGIN_FORM_SUBMIT',"
                 +"   payload: form"
                 +" }", isSelected: false},
          {text: "{"
                  +"   actionType: 'LOGIN_FORM_SUBMIT',"
                  +"   payload: form"
                  +" }", isSelected: false},
          {text: "{"
                  +"   type: 'LOGIN_FORM_SUBMIT',"
                  +"   payload: form"
                  +" }", isSelected: false, isCorrect: true},
          {text: "{"
                  +"   reduce: 'LOGIN_FORM_SUBMIT',"
                  +"   payload: form"
                  +" }", isSelected: false},]
      },
      {
        question_id: 11,
        text: "Какие из методов являются частью библиотеки redux?",
        type: 1,
        answers: [
          {text: "createStore", isSelected: false, isCorrect: true},
          {text: "createReducer", isSelected: false},
          {text: "connectStore", isSelected: false},
          {text: "bindActionCreator", isSelected: false, isCorrect: true}]
      },
    ];





    $scope.questions.forEach(function (item, index) {
      item.name = 'answer_' + index;

      (item.answers || []).forEach(function (ans, indx) {
        ans.uniqId2 = ans.uniqId = 'answer_' + index + '_' + indx;
      });
    });

    $scope.answerIsSelected = function (question) {
      return question.answers.find(function (ans) {
        return ans.isSelected;
      });
    };

    $scope.saveToLocalStorage = function(question, ans){
      console.log(question.question_id, ans.uniqId);
      localStorage.setItem(question.question_id, ans.uniqId);
    };

    $scope.saveAnswers = function () {
      var message = '';

      $scope.questions.forEach(function (item, index) {
        message += (index + 1) + ') ';

        if (item.type === 1) {
          message += item.text + ':<br>';

          message += item.answers.reduce(function (full, current, indx) {
            if (!current.isSelected) {
              return full;
            }

            return full + ' ' + (indx + 1) + ' ' + current.text + '<br>';
          }, '');

          message += '<br>';
        } else if (item.type === 2) {
          message += item.text + ':<br>';
          message += item.selected.text + '<br>';
        }
        else if (item.type === 3) {
          message += item.text + ':<br>';
          message += item.answer.replace(/\n/g, "<br>") + '<br>';
        }
      });

      $scope.quizParams.isSending = true;

      console.log(message);
      console.log($scope.questions);

      /*$http.post('http://modules.dev.b2bpolis.ru/sendmail', {
        content: 'От кого: ' + $scope.quizParams.userEmail + '<br><br>' + message/*{
          type: 'from quiz',
          from: $scope.quizParams.userEmail,
          message: message
        }*/
     /* }).then(function(response) {
        if(response.data.sent === "sent"){
          alert("Сообщение успешно отправлено!");
          $scope.quizParams.isSent = true;
        } else {
          throw response;
        }
      })['catch'](function () {
        alert("Не удалось отправить!");
      })['finally'](function () {
        $scope.quizParams.isSending = false;
      });*/
    };
  }
]);